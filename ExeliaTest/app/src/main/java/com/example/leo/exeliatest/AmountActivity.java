package com.example.leo.exeliatest;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class AmountActivity extends AppCompatActivity {
    private EditText _txtAmount;
    private Button _btnAmount;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_amount);
        _txtAmount=(EditText) findViewById(R.id.txtAmount);
        _btnAmount=(Button) findViewById(R.id.btnPress);
        _btnAmount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String amount = _txtAmount.getText().toString();

                if (amount.isEmpty()) {
                    _txtAmount.setError("Enter a valid Amount");
                    _txtAmount.requestFocus();
                } else {
                final SharedPreferences objpref=getSharedPreferences("ExeliaTest", MODE_PRIVATE);
                objpref.edit().putString("Amount",_txtAmount.getText().toString ()).commit();
                OpenSecondActivity();
                }
            }
        });

    }

    public boolean Validate() {
        boolean valid = true;

        String amount = _txtAmount.getText().toString();

        if (amount.isEmpty()) {
            _txtAmount.setError("enter a valid username");
            _txtAmount.requestFocus();
            valid = false;
        } else {
            _txtAmount.setError(null);
        }

        return valid;
    }

    private void OpenSecondActivity() {
        Intent actVerify = new Intent(AmountActivity.this, SecondActivity.class);
        startActivity(actVerify);
    }
}
