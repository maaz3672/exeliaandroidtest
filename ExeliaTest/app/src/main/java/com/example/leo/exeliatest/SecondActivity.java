package com.example.leo.exeliatest;

import android.content.SharedPreferences;
import android.os.Build;
import android.provider.Contacts;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import static java.lang.String.valueOf;

public class SecondActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private Spinner _spPeoples;
    private TextView _tvPercentage;
    private TextView _tvPay;
    private EditText _txtPercentage;
    private SeekBar _sbPercentageBar;
    int per;
    int persons;
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        _tvPercentage=(TextView)findViewById(R.id.tvPercentage);
        _tvPay=(TextView)findViewById(R.id.tvPay);
        _txtPercentage=(EditText)findViewById(R.id.txtPercentage);
        _sbPercentageBar=(SeekBar) findViewById(R.id.sbPercentageBar);
        _spPeoples=(Spinner)findViewById(R.id.ddpeople);
        List<Integer> spinnerArray = new ArrayList<>();
        for(int i=1; i<=10; i++){
        spinnerArray.add(i);
        }
        ArrayAdapter<Integer> adapter = new ArrayAdapter<Integer>(
                SecondActivity.this,
                android.R.layout.simple_spinner_dropdown_item,
                spinnerArray
        );
        _spPeoples.setAdapter(adapter);
        _spPeoples.setOnItemSelectedListener(this);


        // Edit Text Listener Method
        _txtPercentage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    if(_txtPercentage.getText().toString().length()>0) {
                    per= Integer.parseInt(_txtPercentage.getText().toString());
                    if(per >100){
                        _txtPercentage.setError("You Across Percentage Limit");
                        _txtPercentage.requestFocus();
                    }
                    else {
                        _sbPercentageBar.setProgress(per);
                        _tvPercentage.setText(_txtPercentage.getText().toString() + "%");
                        EachPersonPay();
                    }

                }
                }catch (Exception e){
                    Toast.makeText(SecondActivity.this,  e.toString () , Toast.LENGTH_SHORT).show();
                }
            }
        });

        // SeekBar Method to Set Percentage
        _sbPercentageBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                per=progress;
                _txtPercentage.setText(Integer.toString(progress));
                _tvPercentage.setText(String.valueOf(progress)+" %");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                EachPersonPay();
            }
        });
    }

    // Custom Method to Calculate How Much Each Person Pay
    private void EachPersonPay() {
        SharedPreferences objpref=getSharedPreferences("ExeliaTest", MODE_PRIVATE);
        Integer paidamount;
        Integer totalamount= Integer.parseInt(objpref.getString("Amount","").toString());
        paidamount=totalamount+(((totalamount*per)/100)*persons);
        _tvPay.setText("Each Person Pay "+paidamount.toString());
    }

    // Spinner Method to Select No. Of Persons
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        persons= (Integer) parent.getItemAtPosition(position);
        EachPersonPay();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

}
